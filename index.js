const express = require('express');
const xlsx = require('xlsx2json');

const app = express();
xlsx(
    `${__dirname}/2.xlsx`,
    {
        dataStartingRow: 2,
        mapping: {
            'name': 'A',
            'address': 'B',
            'description': 'C',
            'category': 'D',
            'mobileNo': 'E'
        }
    }
).then(jsonArray => {
    workSheetsFromFile = jsonArray;
 });
app.get('/', function (req, res) {
    res.send(workSheetsFromFile);
});

app.listen(3000);
